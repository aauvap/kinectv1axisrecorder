﻿//------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Microsoft.Samples.Kinect.ColorBasics
{
    using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Drawing;
using Microsoft.Kinect;
using System.Runtime.Serialization.Formatters.Binary;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
    using Emgu.CV;
    using Emgu.CV.Structure;
    using Emgu.Util;

    
    
    

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Active Kinect sensor
        /// </summary>
        /// 
       
        private KinectSensor sensor;
        

        /// <summary>
        /// Bitmap that will hold color + depth information
        /// </summary>
        private WriteableBitmap colorBitmap;
        private WriteableBitmap depthBitmap;
        private WriteableBitmap depthMapX;
        private WriteableBitmap depthMapY;
        public ColorImagePoint[] mappedDepthLocations { get; set; }
        public DepthImagePoint[] mappedRGBLocations { get; set; }
        
        public MainForm axiswindow;
        

        /// <summary>
        /// Subfolders to hold calibration and capture data
        /// </summary>

        private string currentCalFolder;
        private string currentRootFolder;
        public bool DEPTHCAL_CAPTURE_FLAG;
        public bool DEPTH_CAPTURE_FLAG;
        public bool RGB_CAPTURE_FLAG;
        public bool CALIBRATE_RGBT_FLAG;
        public bool ENABLE_THERMAL_FLAG;
        public DateTime captureTimerStartTime;
        System.Windows.Threading.DispatcherTimer captureTimer;

        private int depthFramesCalibrated;

        private string calFolderPrefix = "Cal/";
        private string capFolderPrefix = "Cap/";
        private string capRGBFolder = "RGB/";
        private string capTFolder = "T/";
        private string capDFolder = "D/";

        
        /// <summary>
        /// Intermediate storage for the color data received from the camera
        /// </summary>
        private byte[] colorPixels;
        

        /// <summary>
        /// Intermediate storage for the depth data received from the camera
        /// </summary>
        private DepthImagePixel[] depthPixels;
        private short[] depthPixelsShort; 
        private ushort[] depthLookupX;
        private ushort[] depthLookupY;


        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Execute startup tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            // Look through all sensors and start the first connected one.
            // This requires that a Kinect is connected at the time of app startup.
            // To make your app robust against plug/unplug, 
            // it is recommended to use KinectSensorChooser provided in Microsoft.Kinect.Toolkit
            foreach (var potentialSensor in KinectSensor.KinectSensors)
            {
                if (potentialSensor.Status == KinectStatus.Connected)
                {
                    this.sensor = potentialSensor;
                    break;
                    
                }
            }

            if (null != this.sensor)
            {
                // Turn on the color and depth stream to receive color frames
                this.sensor.ColorStream.Enable(ColorImageFormat.YuvResolution640x480Fps15);
                this.sensor.DepthStream.Enable(DepthImageFormat.Resolution640x480Fps30);

                // Allocate space to put the pixels we'll receive
                this.colorPixels = new byte[this.sensor.ColorStream.FramePixelDataLength];
                this.depthPixelsShort = new short[this.sensor.DepthStream.FramePixelDataLength];
                this.depthPixels = new DepthImagePixel[this.sensor.DepthStream.FramePixelDataLength];
                
                // This is the bitmap we'll display on-screen
                this.colorBitmap = new WriteableBitmap(this.sensor.ColorStream.FrameWidth, this.sensor.ColorStream.FrameHeight, 96.0, 96.0, PixelFormats.Bgr32, null);
                this.depthMapX = new WriteableBitmap(this.sensor.ColorStream.FrameWidth, this.sensor.ColorStream.FrameHeight, 96.0, 96.0, PixelFormats.Gray16, null);
                this.depthMapY = new WriteableBitmap(this.sensor.ColorStream.FrameWidth, this.sensor.ColorStream.FrameHeight, 96.0, 96.0, PixelFormats.Gray16, null);
                this.depthBitmap = new WriteableBitmap(this.sensor.DepthStream.FrameWidth, this.sensor.DepthStream.FrameHeight, 96.0, 96.0, PixelFormats.Gray16, null);
                
                // Set the image we display to point to the bitmap where we'll put the image data
                this.Image.Source = this.colorBitmap;
                this.ENABLE_THERMAL_FLAG = true;

                // Add an event handler to be called whenever there is new color frame data
                EventHandler<AllFramesReadyEventArgs> copy = this.SensorAllFramesReady;
                if (copy != null)
                {
                    this.sensor.AllFramesReady += copy;
                }
                                
                //this.sensor.ColorFrameReady += this.SensorColorFrameReady;
                //this.sensor.DepthFrameReady += this.SensorDepthFrameReady;


                // Start the sensor!
                try
                {
                    this.sensor.Start();
                }
                catch (IOException)
                {
                    this.sensor = null;
                }
                
            }

            if (null == this.sensor)
            {
                this.statusBarText.Text = Properties.Resources.NoKinectReady;
            }
            else
            {
                this.statusBarText.Text = Properties.Resources.KinectConnected;
            }
            
            // Create and start the timer
            this.captureTimer = new System.Windows.Threading.DispatcherTimer();
            this.captureTimer.Tick += new EventHandler(captureTimer_Tick);
            this.captureTimer.Interval = new TimeSpan(0, 0, 1);
            this.captureTimerStartTime = new DateTime(0);
            long k = this.captureTimerStartTime.Ticks;

            // Open the interface for the AXIS thermal camera
            this.axiswindow = new MainForm();
            axiswindow.Show();
            axiswindow.FormClosed += axiswindow_FormClosed;

            //
            this.currentRootFolder = this.textBox1.Text;

            
        }

        void axiswindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.enableThermalCheckBox.IsChecked = false;
            this.axiswindow.Enabled = false;
        }


        /// <summary>
        /// Execute shutdown tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        /// 



        private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                this.axiswindow.Close();
            }
            catch
            {
                Console.WriteLine("Unable to close AxisWindow");
            }

            if (null != this.sensor)
            {
                this.sensor.Stop();

            }
            
        }

        private void SensorAllFramesReady(object sender, AllFramesReadyEventArgs e)
        {
            byte[] newColorPixels = new byte[this.sensor.ColorStream.FramePixelDataLength];
            string timeStamp = System.DateTime.Now.ToString("hh'-'mm'-'ss'-'ffff", CultureInfo.CurrentUICulture.DateTimeFormat);
            bool calibrateDepth = CALIBRATE_RGBT_FLAG;

            if (RGB_CAPTURE_FLAG) // If we are currently recording RGB, use this method
            {
                using (ColorImageFrame colorFrame = e.OpenColorImageFrame())
                {
                    if (colorFrame != null)
                    {
                        // Copy the pixel data from the image to a temporary array
                        colorFrame.CopyPixelDataTo(newColorPixels);
                    }
                }

                // Write the pixel data into our bitmap
                Task<int> rgbTask = Task<int>.Factory.StartNew(() => processColor(newColorPixels, timeStamp,calibrateDepth));

                if (rgbTask.Result == (-1))
                {
                    this.statusBar2Text.Text = "Unable to save RGB frame " + timeStamp.ToString();
                }

            }
            else // If not, we may update the ColorBitmap using the conventional method
            {
                using (ColorImageFrame colorFrame = e.OpenColorImageFrame())
                {
                    if (colorFrame != null)
                    {
                        // Copy the pixel data from the image to a temporary array
                        colorFrame.CopyPixelDataTo(this.colorPixels);
                    }
                }

                // Write the pixel data into our bitmap
                this.colorBitmap.WritePixels(
                new Int32Rect(0, 0, this.colorBitmap.PixelWidth, this.colorBitmap.PixelHeight),
                this.colorPixels,
                this.colorBitmap.PixelWidth * sizeof(int),
                0);
            }

            if (DEPTH_CAPTURE_FLAG) // If we are currently recording depth, save frames to disk
            {
                short[] newDepthPixels = new short[640*480];

                using (DepthImageFrame depthFrame = e.OpenDepthImageFrame())
                {
                    if (depthFrame != null)
                    {
                        depthFrame.CopyPixelDataTo(newDepthPixels);
                    }
                }
                
                // Start a new thread to process the depth images
                Task<int> dTask = Task<int>.Factory.StartNew(() => processDepth(newDepthPixels, timeStamp, calibrateDepth));

                if (dTask.Result == (-1))
                {
                    this.statusBar2Text.Text = "Unable to save depth frame " + timeStamp.ToString();
                }

            } 
            else if (DEPTHCAL_CAPTURE_FLAG) // If we are calibrating depth, use another method
            {
                using (DepthImageFrame depthFrame = e.OpenDepthImageFrame())
                {
                    if (depthFrame != null)
                    {
                        // Copy the pixel data from the image to a temporary array
                        depthFrame.CopyDepthImagePixelDataTo(this.depthPixels);

                        // Write the depth data to a bitmap that we will use in the calibration functions
                        int bytesPerPixel = 2;

                        this.depthBitmap.WritePixels(
                        new Int32Rect(0, 0, this.depthBitmap.PixelWidth, this.depthBitmap.PixelHeight),
                        this.depthPixels,
                        this.depthBitmap.PixelWidth * bytesPerPixel,
                        0);

                        calibrateDepthD2RGB(depthFrame);
                        //calibrateDepthRGB2D(depthFrame);
                        this.depthFramesCalibrated++;

                        if (this.depthFramesCalibrated == 15)
                        {
                            this.DEPTHCAL_CAPTURE_FLAG = false;
                            this.calibrateDepthButton.Content = "Register depth";
                            this.statusBar2Text.Text = "Depth registered successfully";

                            enableRecordingButtons(true, true, true);
                            enableSettingsButtons();
                        }
                        else if (this.depthFramesCalibrated > 15)
                        {
                            this.DEPTHCAL_CAPTURE_FLAG = false;
                            this.calibrateDepthButton.Content = "Register depth";

                            enableRecordingButtons(true, true, true);
                            enableSettingsButtons();
                        }

                    } // End if
                } // End using
            } // End else if

        }


        private void calibrateDepthD2RGB(DepthImageFrame depthFrame)
        {
                        
            this.mappedDepthLocations = new ColorImagePoint[depthFrame.PixelDataLength];
            

            this.sensor.CoordinateMapper.MapDepthFrameToColorFrame(
                DepthImageFormat.Resolution640x480Fps30,
                this.depthPixels,
                ColorImageFormat.RgbResolution640x480Fps30,
                mappedDepthLocations
            );

            depthLookupX = new ushort[640 * 480];
            depthLookupY = new ushort[640 * 480];

            for (int i = 0; i < this.depthPixels.Length; i++)
            {
                ColorImagePoint point = mappedDepthLocations[i];

                if ((point.X >= 0 && point.X < 640) && (point.Y >= 0 && point.Y < 480))
                {

                    int baseIndex = i;

                    depthLookupX[baseIndex] = (ushort)point.X;
                    depthLookupY[baseIndex] = (ushort)(point.Y);
                }
            }

            int bytesPerPixel = depthMapX.Format.BitsPerPixel / 8;

            try
            {
                this.depthMapX.WritePixels(
                    new Int32Rect(0, 0, this.depthMapX.PixelWidth, this.depthMapX.PixelHeight),
                    depthLookupX,
                    this.depthMapX.PixelWidth * bytesPerPixel,
                    0);

                this.depthMapY.WritePixels(
                    new Int32Rect(0, 0, this.depthMapY.PixelWidth, this.depthMapY.PixelHeight),
                    depthLookupY,
                    this.depthMapY.PixelWidth * bytesPerPixel,
                    0);

            }
            catch
            {
                this.statusBar2Text.Text = "Unable to save the depth registration frames";
                this.depthFramesCalibrated = 15;
            }

            
            string times = System.DateTime.Now.ToString("hh'-'mm'-'ss'-'ff", CultureInfo.CurrentUICulture.DateTimeFormat);
            
            string pathD = Path.Combine(this.currentRootFolder, this.calFolderPrefix,this.currentCalFolder,this.capDFolder,"D2RGB-X" + times + ".png");
            //write the new file to disk
            //create frame from the writable bitmap and add to encoder
            BitmapEncoder encoderD = new PngBitmapEncoder();

            encoderD.Frames.Add(BitmapFrame.Create(this.depthMapX));
            using (FileStream fs = new FileStream(pathD, FileMode.Create))
                encoderD.Save(fs);

            BitmapEncoder encoderD2 = new PngBitmapEncoder();
            string pathD2 = Path.Combine(this.currentRootFolder,this.calFolderPrefix,this.currentCalFolder,this.capDFolder,"D2RGB-Y" + times + ".png");
            encoderD2.Frames.Add(BitmapFrame.Create(this.depthMapY));
            using (FileStream fs = new FileStream(pathD2, FileMode.Create))
                encoderD2.Save(fs);

        }

        private void calibrateDepthRGB2D(DepthImageFrame depthFrame)
        {

            this.mappedRGBLocations = new DepthImagePoint[depthFrame.PixelDataLength];
            
            this.sensor.CoordinateMapper.MapColorFrameToDepthFrame(
                ColorImageFormat.RgbResolution640x480Fps30,    
                DepthImageFormat.Resolution640x480Fps30,
                this.depthPixels,
                this.mappedRGBLocations
            );

            depthLookupX = new ushort[640 * 480];
            depthLookupY = new ushort[640 * 480];

            for (int i = 0; i < this.depthPixels.Length; i++)
            {
                DepthImagePoint point = mappedRGBLocations[i];

                if ((point.X >= 0 && point.X < 640) && (point.Y >= 0 && point.Y < 480))
                {

                    int baseIndex = i;

                    depthLookupX[baseIndex] = (ushort)point.X;
                    depthLookupY[baseIndex] = (ushort)(point.Y);
                }
            }

            int bytesPerPixel = depthMapX.Format.BitsPerPixel / 8;

            this.depthMapX.WritePixels(
                new Int32Rect(0, 0, this.depthMapX.PixelWidth, this.depthMapX.PixelHeight),
                depthLookupX,
                this.depthMapX.PixelWidth * bytesPerPixel,
                0);

            this.depthMapY.WritePixels(
                new Int32Rect(0, 0, this.depthMapY.PixelWidth, this.depthMapY.PixelHeight),
                depthLookupY,
                this.depthMapY.PixelWidth * bytesPerPixel,
                0);


            string times = System.DateTime.Now.ToString("hh'-'mm'-'ss'-'ff", CultureInfo.CurrentUICulture.DateTimeFormat);

            string pathD = Path.Combine(this.currentRootFolder, this.calFolderPrefix, this.currentCalFolder, this.capDFolder, "RGB2D-X" + times + ".png");
            //write the new file to disk
            //create frame from the writable bitmap and add to encoder
            BitmapEncoder encoderD = new PngBitmapEncoder();

            encoderD.Frames.Add(BitmapFrame.Create(this.depthMapX));
            using (FileStream fs = new FileStream(pathD, FileMode.Create))
                encoderD.Save(fs);

            BitmapEncoder encoderD2 = new PngBitmapEncoder();
            string pathD2 = Path.Combine(this.currentRootFolder, this.calFolderPrefix, this.currentCalFolder, this.capDFolder, "RGB2D-Y" + times + ".png");
            encoderD2.Frames.Add(BitmapFrame.Create(this.depthMapY));
            using (FileStream fs = new FileStream(pathD2, FileMode.Create))
                encoderD2.Save(fs);

        }

        private void SensorColorFrameReady(object sender, ColorImageFrameReadyEventArgs e)
        {
            if (RGB_CAPTURE_FLAG) // If we are currently recording RGB, make new threads
            {

                using (ColorImageFrame colorFrame = e.OpenColorImageFrame())
                {
                    if (colorFrame != null)
                    {
                        // Copy the pixel data from the image to a temporary array
                        colorFrame.CopyPixelDataTo(this.colorPixels);
                    }
                }

                // Write the pixel data into our bitmap                
                string timeStamp = System.DateTime.Now.ToString("hh'-'mm'-'ss'-'ffff", CultureInfo.CurrentUICulture.DateTimeFormat);

                //TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));
                //Bitmap colorBitmap = (Bitmap)tc.ConvertFrom(this.colorPixels);
                var colorBitmap = new Bitmap(640, 480);
                var data = colorBitmap.LockBits(new Rectangle(System.Drawing.Point.Empty, colorBitmap.Size), ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                Marshal.Copy(this.colorPixels, 0, data.Scan0, this.colorPixels.Length);
                colorBitmap.UnlockBits(data);
                Emgu.CV.Image<Bgr,Byte> colorImage = new Emgu.CV.Image<Bgr,Byte>(colorBitmap);
                

                colorBitmap.Dispose();
                
                //Task.Factory.StartNew(() => processColor(newColorPixels, timeStamp, isColor))
            }
            else // If not, we may update the ColorBitmap using the conventional method
            {
                using (ColorImageFrame colorFrame = e.OpenColorImageFrame())
                {
                    if (colorFrame != null)
                    {

                        // Copy the pixel data from the image to a temporary array
                        colorFrame.CopyPixelDataTo(this.colorPixels);

                    }
                }
                
                // Write the pixel data into our bitmap
                this.colorBitmap.WritePixels(
                new Int32Rect(0, 0, this.colorBitmap.PixelWidth, this.colorBitmap.PixelHeight),
                this.colorPixels,
                this.colorBitmap.PixelWidth * sizeof(int),
                0);
            }
         
        }
                

        private int processColor(Byte[] newColorPixels, string timeStamp,bool calibrateDepth)
        {

            var colorBitmap = new Bitmap(640, 480);
            var data = colorBitmap.LockBits(new Rectangle(System.Drawing.Point.Empty, colorBitmap.Size), ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppRgb);
            Marshal.Copy(newColorPixels, 0, data.Scan0, newColorPixels.Length);
            colorBitmap.UnlockBits(data);

            string path;
            
            if (calibrateDepth)
            {
                path = this.currentRootFolder + this.calFolderPrefix + this.currentCalFolder + this.capRGBFolder + timeStamp + ".jpg";
            }
            else
            {
                path = this.currentRootFolder + this.capFolderPrefix + this.currentCalFolder + this.capRGBFolder + timeStamp + ".jpg";
            }

            try
            {
                colorBitmap.Save(path, System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            catch
            {
                Console.WriteLine("Failed to save file " + timeStamp);
                return -1;
                
            }

            return 0;
        }

        private int processDepth(short[] newDepthPixels, string timeStamp, bool calibrateDepth)
        {
            string path;

            if (calibrateDepth)
            {
                path = this.currentRootFolder + this.calFolderPrefix + this.currentCalFolder + this.capDFolder + timeStamp + ".png";
            }
            else
            {
                path = this.currentRootFolder + this.capFolderPrefix + this.currentCalFolder + this.capDFolder + timeStamp + ".png";
            }
            
            var depthBitmap = new Bitmap(640 * 2, 480, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);

            var data = depthBitmap.LockBits(new Rectangle(System.Drawing.Point.Empty, depthBitmap.Size), ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
            Marshal.Copy(newDepthPixels, 0, data.Scan0, newDepthPixels.Length);
            depthBitmap.UnlockBits(data);
            ColorPalette pal = depthBitmap.Palette;
            
            for (int j = 0; j < 256; j++)
            {
                pal.Entries[j] = System.Drawing.Color.FromArgb(255, j, j, j);
            }

            depthBitmap.Palette = pal;
            

            try
            {
                depthBitmap.Save(path, System.Drawing.Imaging.ImageFormat.Png);
            }
            catch
            {
                Console.WriteLine("Failed to save file " + timeStamp);
                return -1;
            }

            return 0;

        }


        private void SensorDepthFrameReady(object sender, DepthImageFrameReadyEventArgs e)
        {
            
            if (DEPTH_CAPTURE_FLAG) // If we are currently recording depth, make new threads
            {
                
                using (DepthImageFrame depthFrame = e.OpenDepthImageFrame())
                {
                    if (depthFrame != null)
                    {
                        depthFrame.CopyPixelDataTo(this.depthPixelsShort);
                        
                    }
                }
                

                string timeStamp = System.DateTime.Now.ToString("hh'-'mm'-'ss'-'ffff", CultureInfo.CurrentUICulture.DateTimeFormat);

                var depthBitmap = new Bitmap(640*2, 480);
                var data = depthBitmap.LockBits(new Rectangle(System.Drawing.Point.Empty, depthBitmap.Size), ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
                Marshal.Copy(this.depthPixelsShort, 0, data.Scan0, this.depthPixelsShort.Length);
                depthBitmap.UnlockBits(data);

                depthBitmap.Save(this.currentRootFolder + this.capRGBFolder + timeStamp + ".png", System.Drawing.Imaging.ImageFormat.Png);
            }
            else if (DEPTHCAL_CAPTURE_FLAG) // If we are calibrating depth, use another method
            {
                using (DepthImageFrame depthFrame = e.OpenDepthImageFrame())
                {
                    if (depthFrame != null)
                    {
                        // Copy the pixel data from the image to a temporary array
                        //depthFrame.CopyDepthImagePixelDataTo(this.depthPixels);

                        depthFrame.CopyDepthImagePixelDataTo(this.depthPixels);
                        
                        //int bytesPerPixel = depthBitmap.Format.BitsPerPixel / 8;
                        int bytesPerPixel = 2;
                        
                        this.depthBitmap.WritePixels(
                        new Int32Rect(0, 0, this.depthBitmap.PixelWidth, this.depthBitmap.PixelHeight),
                        this.depthPixels,
                        this.depthBitmap.PixelWidth * bytesPerPixel,
                        0);
                    }
                }

                using (DepthImageFrame depthFrame = e.OpenDepthImageFrame())
                {
                    if (depthFrame != null)
                    {
                        calibrateDepthD2RGB(depthFrame);
                        //calibrateDepthRGB2D(depthFrame);
                        this.depthFramesCalibrated++;

                        if (this.depthFramesCalibrated == 15)
                        {
                            this.DEPTHCAL_CAPTURE_FLAG = false;
                            this.calibrateDepthButton.Content = "Register depth";
                            this.statusBar2Text.Text = "Depth registered successfully";
                        }

                    }
                }
            }

            
        }


        private void textBox1_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            if (this.IsInitialized)
            {
                if (textBox1.Text.EndsWith("/"))
                // If the entered string is completed, we may create the folders, thus activating the button
                {
                    this.createFoldersButton.IsEnabled = true;
                    this.currentRootFolder = this.textBox1.Text;
                    this.statusBar2Text.Text = "";
                }
                else
                {
                    this.createFoldersButton.IsEnabled = false;
                    this.statusBar2Text.Text = "The path must end with \"\\\" to create subfolders";

                    disableRecordingButtons(true, true, true);
                }
            }

        }

        private void captureRgbdtButtonClick(object sender, RoutedEventArgs e)
        {
            if (null == this.sensor)
            {
                this.statusBarText.Text = Properties.Resources.ConnectDeviceFirst;
                return;
            }
            
            if (!DEPTH_CAPTURE_FLAG)
            { // Start the recording
                
                // Check if we are overwriting an existing capture

                bool startRecording = false;

                string path = this.currentRootFolder + this.capFolderPrefix + this.currentCalFolder + "thermal.asf";
                if (File.Exists(path))
                {
                    if (System.Windows.Forms.MessageBox.Show("A capture already exists in this folder. Do you want to overwrite it?", 
                        "Previous capture detected", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
                        == System.Windows.Forms.DialogResult.Yes)
                    {
                        startRecording = true;
                    }

                } else {
                    startRecording = true;
                }


                if (startRecording)
                {

                    disableRecordingButtons(true, true, false);
                    disableSettingsButtons();

                    if (ENABLE_THERMAL_FLAG)
                    {
                        this.currentRootFolder = this.textBox1.Text;
                        string calFolder = Path.Combine(this.currentRootFolder, this.capFolderPrefix, this.currentCalFolder);
                        this.axiswindow.correctMyFileBox(calFolder);
                        this.axiswindow.myRecordButton_Click(sender, e);

                        // Start the timer
                        this.captureTimer.Start();
                        this.captureTimerStartTime = DateTime.MinValue;

                        if (this.axiswindow.isStreamOpened && this.axiswindow.isRecording) // If we have succeeded recording on the thermal camera, go further
                        {
                            RGB_CAPTURE_FLAG = true;
                            DEPTH_CAPTURE_FLAG = true;
                            GlobalClass.RGBDT_CAPTURE_FLAG_SET = true;
                            this.captureRgbdtButton.Content = "Stop capture";
                            this.statusBar2Text.Text = "Capturing RGB + D + T";
                            this.Image.Source = null;
                        }
                        else
                        {
                            this.statusBar2Text.Text = "Failed to record from thermal stream";

                            this.captureTimer.Stop();
                            enableRecordingButtons(true, true, true);
                            enableSettingsButtons();
                        }
                    }
                    else
                    {
                        RGB_CAPTURE_FLAG = true;
                        DEPTH_CAPTURE_FLAG = true;

                        // Start the timer
                        this.captureTimer.Start();
                        this.captureTimerStartTime = DateTime.MinValue;

                        this.captureRgbdtButton.Content = "Stop capture";
                        this.statusBar2Text.Text = "Capturing RGB + D";
                        this.Image.Source = null;
                    }
                }

            }
            else // Stop the recording
            {
                enableRecordingButtons(true, true, true);
                enableSettingsButtons();

                if (ENABLE_THERMAL_FLAG)
                {
                    this.axiswindow.myRecordButton_Click(sender, e);
                    RGB_CAPTURE_FLAG = false;
                    DEPTH_CAPTURE_FLAG = false;
                    GlobalClass.RGBDT_CAPTURE_FLAG_SET = false;
                    this.captureTimer.Stop();

                    if (!this.axiswindow.isRecording)
                    {

                        this.captureRgbdtButton.Content = Properties.Resources.CaptureButtonThemalEn;
                        this.statusBar2Text.Text = "Capture stopped";
                        this.Image.Source = this.colorBitmap;
                    }
                    else
                    {
                        this.statusBar2Text.Text = "Failed to stop the thermal recording. RGB and D streams are stopped";
                        this.captureRgbdtButton.Content = Properties.Resources.CaptureButtonThemalEn;
                    }
                }
                else
                {
                    RGB_CAPTURE_FLAG = false;
                    DEPTH_CAPTURE_FLAG = false;

                    this.captureTimer.Stop();
                    this.captureRgbdtButton.Content = Properties.Resources.CaptureButtonThemalUn;
                    this.statusBar2Text.Text = "Capture stopped";
                    this.Image.Source = this.colorBitmap;
                    
                }
            }

        }

        private void calibrateDepthButton_Click(object sender, RoutedEventArgs e)
        {
            if (null == this.sensor)
            {
                this.statusBarText.Text = Properties.Resources.ConnectDeviceFirst;
                return;
            }


            if (DEPTHCAL_CAPTURE_FLAG)
            {
                // Stop the depth registration
                this.DEPTHCAL_CAPTURE_FLAG = false;
                this.calibrateDepthButton.Content = "Register depth";

                enableRecordingButtons(true, true, true);
                enableSettingsButtons();
            }
            else
            {
                // Initialize the depth registration
                this.currentRootFolder = this.textBox1.Text;
                this.depthFramesCalibrated = 0;
                this.DEPTHCAL_CAPTURE_FLAG = true;
                this.calibrateDepthButton.Content = "Stop registration";
                this.statusBar2Text.Text = "Registering depth... please wait";

                disableRecordingButtons(false, true, true);
                disableSettingsButtons();
            }
        }

        private void calibrateRgbtButton_Click(object sender, RoutedEventArgs e)
        {
            if (null == this.sensor)
            {
                this.statusBarText.Text = Properties.Resources.ConnectDeviceFirst;
                return;
            }


            if (!RGB_CAPTURE_FLAG)
            { // Start the recording

                bool startRecording = false;

                string path = this.currentRootFolder + this.calFolderPrefix + this.currentCalFolder + "thermal.asf";
                if (File.Exists(path))
                {
                    if (System.Windows.Forms.MessageBox.Show("A calibration capture already exists in this folder. Do you want to overwrite it?",
                        "Previous calibration capture detected", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
                        == System.Windows.Forms.DialogResult.Yes)
                    {
                        startRecording = true;
                    }

                }
                else
                {
                    startRecording = true;
                }


                if (startRecording)
                {
                    disableRecordingButtons(true, false, true);
                    disableSettingsButtons();

                    if (ENABLE_THERMAL_FLAG)
                    {
                        // Make sure the thermal camera is recording in the right folder                
                        this.currentRootFolder = this.textBox1.Text;
                        string calFolder = Path.Combine(this.currentRootFolder, this.calFolderPrefix, this.currentCalFolder);
                        this.axiswindow.correctMyFileBox(calFolder);
                        this.axiswindow.myRecordButton_Click(sender, e);

                        // Start the timer
                        this.captureTimer.Start();
                        this.captureTimerStartTime = DateTime.MinValue;

                        if (this.axiswindow.isStreamOpened && this.axiswindow.isRecording) // If we have succeeded recording on the thermal camera, go further
                        {
                            RGB_CAPTURE_FLAG = true;
                            CALIBRATE_RGBT_FLAG = true;
                            GlobalClass.RGBDT_CAPTURE_FLAG_SET = true;
                            DEPTH_CAPTURE_FLAG = true;
                            this.calibrateRgbtButton.Content = "Stop capture";
                            this.statusBar2Text.Text = "Capturing RGB + T calibration sequences";
                        }
                        else
                        {
                            this.statusBar2Text.Text = "Failed to record from thermal stream";
                            this.captureTimer.Stop();
                            enableRecordingButtons(true, true, true);
                            enableSettingsButtons();
                        }

                    } else
                    {
                        RGB_CAPTURE_FLAG = true;
                        CALIBRATE_RGBT_FLAG = true;
                        DEPTH_CAPTURE_FLAG = true;
                        this.captureTimer.Start();
                        this.captureTimerStartTime = DateTime.MinValue;

                        this.calibrateRgbtButton.Content = "Stop capture";
                        this.statusBar2Text.Text = "Capturing RGB calibration sequences";
                    }
                }

            }
            else // Stop the recording
            {
                if (ENABLE_THERMAL_FLAG)
                {
                    this.axiswindow.myRecordButton_Click(sender, e);
                    RGB_CAPTURE_FLAG = false;
                    CALIBRATE_RGBT_FLAG = false;
                    DEPTH_CAPTURE_FLAG = false;
                    GlobalClass.RGBDT_CAPTURE_FLAG_SET = false;
                    this.captureTimer.Stop();

                    if (!this.axiswindow.isRecording)
                    {
                        this.calibrateRgbtButton.Content = Properties.Resources.CalibrateButtonThermalEn;
                        this.statusBar2Text.Text = "Calibration capture stopped";
                    }
                    else
                    {
                        this.statusBar2Text.Text = "Failed to stop the thermal recording";
                    }
                }
                else
                {
                    RGB_CAPTURE_FLAG = false;
                    CALIBRATE_RGBT_FLAG = false;
                    DEPTH_CAPTURE_FLAG = false;
                    this.captureTimer.Stop();
                    this.calibrateRgbtButton.Content = Properties.Resources.CalibrateButtonThermalUn;
                    this.statusBar2Text.Text = "Calibration capture stopped";
                }

                enableRecordingButtons(true, true, true);
                enableSettingsButtons();
            }
            
        }

        private void createFoldersButton_Click(object sender, RoutedEventArgs e)
        {
            createAndSetFolders();
        }

        private void createAndSetFolders(bool createFolders = true)
        {
            try
            {
                for (int i = 1; i <= 5; i++)
                {
                    // Create calibration folders:
                    string path = Path.Combine(this.textBox1.Text, this.calFolderPrefix, i.ToString(), this.capRGBFolder);
                    Directory.CreateDirectory(path);

                    path = Path.Combine(this.textBox1.Text, this.calFolderPrefix, i.ToString(), this.capDFolder);
                    Directory.CreateDirectory(path);

                    path = Path.Combine(this.textBox1.Text, this.calFolderPrefix, i.ToString(), this.capTFolder);
                    Directory.CreateDirectory(path);

                    // Create capture folders:
                    path = Path.Combine(this.textBox1.Text, this.capFolderPrefix, i.ToString(), this.capRGBFolder);
                    Directory.CreateDirectory(path);

                    path = Path.Combine(this.textBox1.Text, this.capFolderPrefix, i.ToString(), this.capDFolder);
                    Directory.CreateDirectory(path);

                    path = Path.Combine(this.textBox1.Text, this.capFolderPrefix, i.ToString(), this.capTFolder);
                    Directory.CreateDirectory(path);
                }

                //string path2 = Path.Combine(this.textBox1.Text, this.capRGBFolder);
                //Directory.CreateDirectory(path2);
                //path2 = Path.Combine(this.textBox1.Text, this.capDFolder);
                //Directory.CreateDirectory(path2);
                //path2 = Path.Combine(this.textBox1.Text, this.capTFolder);
                //Directory.CreateDirectory(path2);

                if (this.ENABLE_THERMAL_FLAG)
                {
                    this.axiswindow.correctMyFileBox(this.textBox1.Text);
                }
                this.statusBar2Text.Text = "Subfolders created";

                enableRecordingButtons(true, true, true);
            }
            catch
            {
                this.statusBar2Text.Text = "Unable to create folders";

                disableRecordingButtons(true, true, true);
            }

        }

        void enableRecordingButtons(bool button1, bool button2, bool button3)
        {
            if (button1)
            {
                this.calibrateDepthButton.Opacity = 1;
                this.calibrateDepthButton.IsEnabled = true;
            }

            if (button2)
            {
                this.calibrateRgbtButton.Opacity = 1;
                this.calibrateRgbtButton.IsEnabled = true;
            }

            if (button3)
            {
                this.captureRgbdtButton.Opacity = 1;
                this.captureRgbdtButton.IsEnabled = true;
            }

        }

        void disableRecordingButtons(bool button1, bool button2, bool button3)
        {
            if (button1)
            {
                this.calibrateDepthButton.Opacity = 0.5;
                this.calibrateDepthButton.IsEnabled = false;
            }

            if (button2)
            {
                this.calibrateRgbtButton.Opacity = 0.5;
                this.calibrateRgbtButton.IsEnabled = false;
            }

            if (button3)
            {
                this.captureRgbdtButton.Opacity = 0.5;
                this.captureRgbdtButton.IsEnabled = false;
            }
        }

        void enableSettingsButtons()
        {
            this.groupBox1.IsEnabled = true;
            this.groupBox2.IsEnabled = true;
            this.textBox1.IsEnabled = true;
            this.createFoldersButton.IsEnabled = true;

            if (this.axiswindow.Enabled)
            {
                this.axiswindow.enableControls();
            }
        }

        void disableSettingsButtons()
        {
            this.groupBox1.IsEnabled = false;
            this.groupBox2.IsEnabled = false;
            this.textBox1.IsEnabled = false;
            this.createFoldersButton.IsEnabled = false;

            if (this.axiswindow.Enabled)
            {
                this.axiswindow.disableControls();
            }          
            
        }


        private void radioButton1_Checked(object sender, RoutedEventArgs e)
        {
            this.currentCalFolder = "1/";
        }

        private void radioButton2_Checked(object sender, RoutedEventArgs e)
        {
            this.currentCalFolder = "2/";
        }

        private void radioButton3_Checked(object sender, RoutedEventArgs e)
        {
            this.currentCalFolder = "3/";
        }


        private void radioButton4_Checked(object sender, RoutedEventArgs e)
        {
            this.currentCalFolder = "4/";
        }

        private void radioButton5_Checked(object sender, RoutedEventArgs e)
        {
            this.currentCalFolder = "5/";
        }


        private void enableThermalCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (this.IsInitialized)
            {
                this.ENABLE_THERMAL_FLAG = true;
                this.calibrateRgbtButton.Content = Properties.Resources.CalibrateButtonThermalEn;
                this.captureRgbdtButton.Content = Properties.Resources.CaptureButtonThemalEn;

                if (!this.axiswindow.Enabled)
                {
                    this.axiswindow = new MainForm();
                    this.axiswindow.Show();
                    this.axiswindow.Enabled = true;
                    this.axiswindow.FormClosed += axiswindow_FormClosed;
                }
            }
        }

        private void enableThermalCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            if (this.IsInitialized)
            {
                this.ENABLE_THERMAL_FLAG = false;
                
                this.calibrateRgbtButton.Content = Properties.Resources.CalibrateButtonThermalUn;
                this.captureRgbdtButton.Content = Properties.Resources.CaptureButtonThemalUn;
            }
        }

        private void enableNearModeCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (this.IsInitialized)
            {
                try
                {
                    this.sensor.DepthStream.Range = DepthRange.Near;
                }
                catch (InvalidOperationException)
                {
                    this.statusBarText.Text = "Unable to intialize near mode";
                    this.enableNearModeCheckBox.IsChecked = false;
                }
            }
        }

        private void enableNearModeCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            if (this.IsInitialized)
            {
                try
                {
                    this.sensor.DepthStream.Range = DepthRange.Default;
                }
                catch
                {
                    this.statusBarText.Text = "Near mode is not supported on Kinect for Xbox";
                }
            }
        }

        private void captureTimer_Tick(object sender, EventArgs e)
        {
            if (this.captureTimerStartTime.Ticks == 0)
            {    // Initialize the timer
                this.captureTimerStartTime = System.DateTime.Now;
                timerBox.Text = "00:00";
                timerBox.Foreground = System.Windows.Media.Brushes.Black;
            }
            else
            {
                TimeSpan duration = DateTime.Now - this.captureTimerStartTime;
                timerBox.Text = duration.ToString(@"mm\:ss");

                if (duration.Minutes >= 5)
                {
                    timerBox.Foreground = System.Windows.Media.Brushes.Red;
                }
            }
            
            
        }





 
        











        
    }
}
using System;
using System.Drawing;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
using System.Data;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.Util;

namespace Microsoft.Samples.Kinect.ColorBasics
{
	/// <summary>
	/// Summary description for MainForm.
	/// </summary>
    public class MainForm : System.Windows.Forms.Form
    {
        public AxAXISMEDIACONTROLLib.AxAxisMediaControl myAMC;
        private System.Windows.Forms.Button myPlayButton;
        private System.Windows.Forms.TextBox myUrlBox;
        private System.Windows.Forms.ComboBox myTypeBox;
        private System.Windows.Forms.Button myStopButton;
        private System.Windows.Forms.TextBox myFileBox;
        private System.Windows.Forms.TextBox myPassBox;
        private System.Windows.Forms.TextBox myUserBox;
        private System.Windows.Forms.Label userLabel;
        private System.Windows.Forms.Label passLabel;
        public bool isRecording { get; private set; }
        public bool isStreamOpened { get; private set; }

        // Declare variables
        List<Image<Gray, byte>> imageList = new List<Image<Gray, byte>>();
        string prevTime = "";
        
        List<string> timeIndices = new List<string>();
        private Button myFindIpButton;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        public MainForm()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.myAMC = new AxAXISMEDIACONTROLLib.AxAxisMediaControl();
            this.myPlayButton = new System.Windows.Forms.Button();
            this.myUrlBox = new System.Windows.Forms.TextBox();
            this.myFileBox = new System.Windows.Forms.TextBox();
            this.myTypeBox = new System.Windows.Forms.ComboBox();
            this.myStopButton = new System.Windows.Forms.Button();
            this.myPassBox = new System.Windows.Forms.TextBox();
            this.myUserBox = new System.Windows.Forms.TextBox();
            this.userLabel = new System.Windows.Forms.Label();
            this.passLabel = new System.Windows.Forms.Label();
            this.myFindIpButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.myAMC)).BeginInit();
            this.SuspendLayout();
            this.isRecording = false;
            this.isStreamOpened = false;

            // 
            // myAMC
            // 
            this.myAMC.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.myAMC.Enabled = true;
            this.myAMC.Location = new System.Drawing.Point(8, 8);
            this.myAMC.Name = "myAMC";
            this.myAMC.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("myAMC.OcxState")));
            this.myAMC.Size = new System.Drawing.Size(368, 312);
            this.myAMC.TabIndex = 0;
            this.myAMC.TabStop = false;
            this.myAMC.OnError += new AxAXISMEDIACONTROLLib._IAxisMediaControlEvents_OnErrorEventHandler(this.myAMC_OnError);
            this.myAMC.OnNewImage += new System.EventHandler(this.myAMC_OnNewImage);
            // 
            // myPlayButton
            // 
            this.myPlayButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.myPlayButton.Location = new System.Drawing.Point(384, 144);
            this.myPlayButton.Name = "myPlayButton";
            this.myPlayButton.Size = new System.Drawing.Size(168, 23);
            this.myPlayButton.TabIndex = 5;
            this.myPlayButton.Text = "Open Thermal stream";
            this.myPlayButton.Click += new System.EventHandler(this.myPlayButton_Click);
            // 
            // myUrlBox
            // 
            this.myUrlBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.myUrlBox.Location = new System.Drawing.Point(384, 8);
            this.myUrlBox.Name = "myUrlBox";
            this.myUrlBox.Size = new System.Drawing.Size(168, 20);
            this.myUrlBox.TabIndex = 1;
            this.myUrlBox.Text = "169.254.183.195";
            // 
            // myFileBox
            // 
            this.myFileBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.myFileBox.Enabled = false;
            this.myFileBox.Location = new System.Drawing.Point(384, 240);
            this.myFileBox.Name = "myFileBox";
            this.myFileBox.Size = new System.Drawing.Size(164, 20);
            this.myFileBox.TabIndex = 9;
            this.myFileBox.Text = "C:/Capture/1/Thermal.asf";
            this.myFileBox.TextChanged += new System.EventHandler(this.myFileBox_TextChanged);
            // 
            // myTypeBox
            // 
            this.myTypeBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.myTypeBox.Items.AddRange(new object[] {
            "mjpeg",
            "mpeg4",
            "h264",
            "mpeg2-unicast",
            "mpeg2-multicast"});
            this.myTypeBox.Location = new System.Drawing.Point(384, 116);
            this.myTypeBox.Name = "myTypeBox";
            this.myTypeBox.Size = new System.Drawing.Size(168, 21);
            this.myTypeBox.TabIndex = 4;
            this.myTypeBox.Text = "mjpeg";
            // 
            // myStopButton
            // 
            this.myStopButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.myStopButton.Location = new System.Drawing.Point(384, 176);
            this.myStopButton.Name = "myStopButton";
            this.myStopButton.Size = new System.Drawing.Size(168, 23);
            this.myStopButton.TabIndex = 7;
            this.myStopButton.Text = "Stop";
            this.myStopButton.Click += new System.EventHandler(this.myStopButton_Click);
            // 
            // myPassBox
            // 
            this.myPassBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.myPassBox.Location = new System.Drawing.Point(472, 92);
            this.myPassBox.Name = "myPassBox";
            this.myPassBox.PasswordChar = '*';
            this.myPassBox.Size = new System.Drawing.Size(80, 20);
            this.myPassBox.TabIndex = 3;
            this.myPassBox.Text = "hans";
            // 
            // myUserBox
            // 
            this.myUserBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.myUserBox.Location = new System.Drawing.Point(472, 68);
            this.myUserBox.Name = "myUserBox";
            this.myUserBox.Size = new System.Drawing.Size(80, 20);
            this.myUserBox.TabIndex = 2;
            this.myUserBox.Text = "root";
            // 
            // userLabel
            // 
            this.userLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.userLabel.Location = new System.Drawing.Point(384, 68);
            this.userLabel.Name = "userLabel";
            this.userLabel.Size = new System.Drawing.Size(80, 20);
            this.userLabel.TabIndex = 8;
            this.userLabel.Text = "Username:";
            this.userLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // passLabel
            // 
            this.passLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.passLabel.Location = new System.Drawing.Point(384, 92);
            this.passLabel.Name = "passLabel";
            this.passLabel.Size = new System.Drawing.Size(80, 20);
            this.passLabel.TabIndex = 8;
            this.passLabel.Text = "Password:";
            this.passLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // myFindIpButton
            // 
            this.myFindIpButton.Location = new System.Drawing.Point(384, 33);
            this.myFindIpButton.Name = "myFindIpButton";
            this.myFindIpButton.Size = new System.Drawing.Size(164, 23);
            this.myFindIpButton.TabIndex = 10;
            this.myFindIpButton.Text = "Find IP address";
            this.myFindIpButton.UseVisualStyleBackColor = true;
            this.myFindIpButton.Click += new System.EventHandler(this.myFindIpButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(560, 326);
            this.Controls.Add(this.myFindIpButton);
            this.Controls.Add(this.userLabel);
            this.Controls.Add(this.myStopButton);
            this.Controls.Add(this.myTypeBox);
            this.Controls.Add(this.myFileBox);
            this.Controls.Add(this.myUrlBox);
            this.Controls.Add(this.myPassBox);
            this.Controls.Add(this.myUserBox);
            this.Controls.Add(this.myPlayButton);
            this.Controls.Add(this.myAMC);
            this.Controls.Add(this.passLabel);
            this.Name = "MainForm";
            this.Text = "Video Sample";
            ((System.ComponentModel.ISupportInitialize)(this.myAMC)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }



        #endregion

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        //static void Main()
        //{
        //    Application.Run(new MainForm());
        //}

        public void myPlayButton_Click(object sender, System.EventArgs e)
        {
            openThermalStream();
        }

        public void openThermalStream()
        {
            try
            {
                // Set properties, deciding what url completion to use by MediaType.
                myAMC.MediaUsername = myUserBox.Text;
                myAMC.MediaPassword = myPassBox.Text;
                myAMC.MediaType = myTypeBox.Text;
                myAMC.MediaURL = CompleteURL(myUrlBox.Text, myTypeBox.Text);

                // Start the streaming
                myAMC.Play();
                isStreamOpened = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Unable to play stream: " + ex.Message);
                isStreamOpened = false;
            }
        }

        public void correctMyFileBox(string savePath)
        {
            myFileBox.Text = savePath;
            myFileBox.AppendText("Thermal.asf");
        }

        public void enableControls()
        {
            myPlayButton.Enabled = true;
            myStopButton.Enabled = true;
            myUrlBox.Enabled = true;
            myTypeBox.Enabled = true;
            myUserBox.Enabled = true;
            myPassBox.Enabled = true;
            myFindIpButton.Enabled = true;
        }

        public void disableControls()
        {
            myPlayButton.Enabled = false;
            myStopButton.Enabled = false;
            myUrlBox.Enabled = false;
            myTypeBox.Enabled = false;
            myUserBox.Enabled = false;
            myPassBox.Enabled = false;
            myFindIpButton.Enabled = false;
        }

        private void myPlayFileButton_Click(object sender, System.EventArgs e)
        {
            // Set the MediaFile property to the selected file. Mediatype does not
            // matter when playing a file.
            FileDialog aDialog = new OpenFileDialog();
            if (aDialog.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    myAMC.MediaFile = aDialog.FileName;
                    myAMC.Play();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(this, "Unable to play file: " + ex.Message);
                }
            }
        }

        private void myStopButton_Click(object sender, System.EventArgs e)
        {
            try
            {
                // Stop the stream (will also stop any recording in progress).
                myAMC.Stop();
                this.isRecording = false;
                isStreamOpened = false;
                //myRecordButton.Text = "Record";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                this.isRecording = false;
                isStreamOpened = false;
            }
            
        }

        public void myRecordButton_Click(object sender, System.EventArgs e)
        {
            try
            {
                if (this.isRecording)
                {
                    // Stop the recording
                    myAMC.StopRecordMedia();
                    //myRecordButton.Text = "Record";
                    this.isRecording = false;
                                        
                    // Write the log of the timeIndices to file - and then clear the timeIndices
                    writeLogToFile(timeIndices);
                    timeIndices.Clear();
                }
                else
                {
                    // Start the recording
                    // The flag 8 means that only video will be recorded.
                    string time = System.DateTime.Now.ToString("hh'-'mm'-'ss'-'ff", CultureInfo.CurrentUICulture.DateTimeFormat);
                    string modifiedSavePath = myFileBox.Text.Replace(".asf","") + "" + time + ".jpg";
                    myAMC.SaveCurrentImage(0, modifiedSavePath);
                    
                    myAMC.StartRecordMedia(myFileBox.Text, 8, "");
                    
                    //myRecordButton.Text = "Stop Recording";

                    this.isRecording = true;
                    
                }
            }
            catch (Exception ex)
            {
                this.isRecording = false;
                MessageBox.Show(this, "Unable to start/stop recording: " + ex.Message);
                
            }
        }

        private void myFileDialogButton_Click(object sender, System.EventArgs e)
        {
            FileDialog aDialog = new OpenFileDialog();
            if (aDialog.ShowDialog(this) == DialogResult.OK)
            {
                myFileBox.Text = aDialog.FileName;
            }
        }

        private string CompleteURL(string theMediaURL, string theMediaType)
        {
            string anURL = theMediaURL;
            if (!anURL.EndsWith("/")) anURL += "/";

            if (theMediaType == "mjpeg")
            {
                anURL += "axis-cgi/mjpg/video.cgi";
            }
            else if (theMediaType == "mpeg4")
            {
                anURL += "mpeg4/media.amp";
            }
            else if (theMediaType == "h264")
            {
                anURL += "axis-media/media.amp?videocodec=h264";
            }
            else if (theMediaType == "mpeg2-unicast")
            {
                anURL += "axis-cgi/mpeg2/video.cgi";
            }
            else if (theMediaType == "mpeg2-multicast")
            {
                anURL += "axis-cgi/mpeg2/video.cgi";
            }

            anURL = CompleteProtocol(anURL, theMediaType);
            return anURL;
        }

        private string CompleteProtocol(string theMediaURL, string theMediaType)
        {
            if (theMediaURL.IndexOf("://") >= 0) return theMediaURL;

            string anURL = theMediaURL;

            if (theMediaType == "mjpeg")
            {
                anURL = "http://" + anURL;
            }
            else if (theMediaType == "mpeg4" || theMediaType == "h264")
            {
                anURL = "axrtsphttp://" + anURL;
            }
            else if (theMediaType == "mpeg2-unicast")
            {
                anURL = "http://" + anURL;
            }
            else if (theMediaType == "mpeg2-multicast")
            {
                anURL = "axsdp" + anURL;
            }

            return anURL;
        }

        private void myAMC_OnError(object sender, AxAXISMEDIACONTROLLib._IAxisMediaControlEvents_OnErrorEvent e)
        {
            //System.Windows.Forms.MessageBox.Show(this, e.theErrorInfo, "Error code " + e.theErrorCode.ToString("X8"));
        }


        void myAMC_OnNewImage(object sender, EventArgs e)
        {
            // Check if capture flag is set
            if (this.isRecording)
            {
                string time = System.DateTime.Now.ToString("hh'-'mm'-'ss'-'ffff", CultureInfo.CurrentUICulture.DateTimeFormat);
                if (time != prevTime)
                {
                    timeIndices.Add(time);
                }
                time = prevTime;
            }
        }

        private byte[, ,] getCurrentFrameAsByte(ref AxAXISMEDIACONTROLLib.AxAxisMediaControl myAMC, int width, int height)
        {
            int streamHeight = height;
            int streamWidth = width;
            object buf = null;
            int countsize = 0;
            int imagesize = streamHeight * streamWidth;
            byte[, ,] data = new byte[streamHeight, streamWidth, 1];

            try
            {
                myAMC.GetCurrentImage(0, out buf, out countsize);
                Array a = (Array)buf;
                byte[] b = new byte[imagesize * 3];
                Buffer.BlockCopy(a, 40, b, 0, imagesize * 3);
                int counter = 0;
                for (int y = streamHeight - 1; y >= 0; y--)
                {
                    for (int x = 0; x < streamWidth; x++)
                    {
                        data[y, x, 0] = b[counter];
                        counter = counter + 3;
                    }
                }
            }
            catch (Exception r)
            {
                //Debug.WriteLine("A non-fatal error occured while trying to capture a frame! Continuing..\nError code: " + r.Message);
            }

            return data;
        }

        private void writeLogToFile(List<string> inputList)
        {
            try
            {

                int i = inputList.Count;
                               
                // use ,true if you want to append data to file

                FileDialog oDialog = new SaveFileDialog();

                oDialog.DefaultExt = "log";
                string fileName = "Timestamps.log";
                oDialog.FileName = myFileBox.Text.Replace(".asf", "") + fileName;



                using (System.IO.StreamWriter file = new System.IO.StreamWriter(@oDialog.FileName))

                foreach (string line in inputList)
                {
                   file.WriteLine(line);
                }

                

            }

            catch (Exception exp)
            {

                MessageBox.Show("" + exp);

            }
        }



        private void myUrlBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void myFileBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void myFindIpButton_Click(object sender, EventArgs e)
        {
            try
            {
                myUrlBox.Text = AxisUPnPSearch.Searcher.GetIpAddress();
            }
            catch (Exception exp)
            {
                MessageBox.Show("" + exp);
            }
            openThermalStream();
        }


    }
}

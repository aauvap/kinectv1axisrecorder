﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microsoft.Samples.Kinect.ColorBasics
{
    class GlobalClass
    {
    
        private static bool RGBT_CAPTURE_FLAG = false;
        private static bool RGBDT_CAPTURE_FLAG = false;
        
        public static bool RGBT_CAPTURE_FLAG_SET
        {
            get {return RGBT_CAPTURE_FLAG;}
            set { RGBT_CAPTURE_FLAG = value; }
        }

        public static bool RGBDT_CAPTURE_FLAG_SET
        {
            get { return RGBDT_CAPTURE_FLAG; }
            set { RGBDT_CAPTURE_FLAG = value; }
        }

        
    }
}
